// @generated by protoc-gen-es v1.10.0
// @generated from file edu/v1/edu.proto (package edu.v1, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { proto3 } from "@bufbuild/protobuf";
import { User } from "./edu_types_pb.js";

/**
 * @generated from message edu.v1.CreateUserRequest
 */
export const CreateUserRequest = /*@__PURE__*/ proto3.makeMessageType(
  "edu.v1.CreateUserRequest",
  () => [
    { no: 1, name: "user", kind: "message", T: User },
  ],
);

/**
 * @generated from message edu.v1.CreateUserResponse
 */
export const CreateUserResponse = /*@__PURE__*/ proto3.makeMessageType(
  "edu.v1.CreateUserResponse",
  [],
);

/**
 * @generated from message edu.v1.GetUserRequest
 */
export const GetUserRequest = /*@__PURE__*/ proto3.makeMessageType(
  "edu.v1.GetUserRequest",
  () => [
    { no: 1, name: "id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
  ],
);

/**
 * @generated from message edu.v1.GetUserResponse
 */
export const GetUserResponse = /*@__PURE__*/ proto3.makeMessageType(
  "edu.v1.GetUserResponse",
  () => [
    { no: 1, name: "user", kind: "message", T: User },
  ],
);

